.. SecKit SA IDM Windows documentation master file, created by

.. _install:

================================================
Installation
================================================

Splunk Enterprise
-------------------------------------------


Download
+++++++++++++++++++++++++++++++++++++++++++

This add on is installed on the Splunk Enterprise Security Search head.

Splunk Enterprise:

- Download the latest published release from `<https://splunkbase.splunk.com/app/4226/>`__
- Download the latest master build from `bitbucket <https://bitbucket.org/SPLServices/SecKit_TA_idm_linux/addon/com.releasebucket/releases>`_


Installation
+++++++++++++++++++++++++++++++++++++++++++


See `installing apps <http://docs.splunk.com/Documentation/AddOns/released/Overview/Wheretoinstall>`_


- Install on Search Head(s)
- Install on Indexers and Intermediate Forwarders


Splunk Cloud:
-------------------------------------------

Using a service request ask for the app installation SecKit_TA_idm_linux id "4226" specify version 1.0 or later


Data Collection (Splunk Enterprise & Splunk Cloud)
----------------------------------------------------


- Expand the archive and install in the deployment-apps server.
  - Copy the inputs stanza from default to local
  - Set ``disabled=false``
  - Set a index we suggest ``oswinscripts``
- Add the app to a deployment server class deployed to all Windows OS UFs restart is required. If using "SecKit TA Windows guidance" utilize ``seckit_all_2_os_windows_0``


Verify Installation
-------------------------------------------

Run the following search for last 30 min records should be returned

``index=<selectedindex> sourcetype="seckit:idm:windows:interface"``
