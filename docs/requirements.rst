.. SecKit SA IDM Windows documentation master file, created by

.. _requirements:

================================================
Splunk System Requirements
================================================

Mandatory
-------------------------------------------
- Splunk Enterprise >7.1.0
- Splunk Enterprise Security >5.1.0
- SecKit IDM Common >=3.0.0
- Splunk TA Windows >=5.0.1
