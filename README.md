# Security Kit #

##Identity Management Windows Components##
 SecKit_TA_idm_linux

### Description ###

This purpose of this Splunk add on is to provide data collection for addition details used by windows assets. The add on collects detailed network configuration from windows hosts via the UF


### Installation ###

[Package Readme](src/SecKit_TA_idm_linux/README.md)

## Contributing ##

PRs require copyright assignment for contributors not employed by Splunk Inc.

## Support ##

Direct contact Community Support as best effort Ryan Faircloth rfarcloth@splunk.com

[Issue Tracker](https://bitbucket.org/SPLServices/SecKit_TA_idm_linux/issues?status=new&status=open)

### Source ###

[bitbucket](https://bitbucket.org/SPLServices/SecKit_TA_idm_linux)
