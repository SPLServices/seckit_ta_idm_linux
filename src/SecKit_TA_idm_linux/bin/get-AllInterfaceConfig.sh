#!/bin/bash

# returns default network interface (DNI) from routing table
DNI=`ip -d route | grep default | cut -d" " -f6`

# returns nameserver's from resolv.conf
RESOLV=`cat /etc/resolv.conf | egrep -v '^\s*#' | sed -e 's/\s\{1,\}/ /' | tr ' ' '='`
read -r DOMAIN IPv4DNS IPv6DNS <<<$(echo $RESOLV | awk '
        {for (i=1; i <= NF; ++i) {split($i,x,"=");
                if( x[1] == "search" ) if(d) d=d "," x[2]; else d=x[2];
                if( x[1] == "nameserver" && x[2] ~ /[0-9]+\./ ) if(ip4) ip4=ip4 "," x[2]; else ip4=x[2];
                if( x[1] == "nameserver" && x[2] ~ /[0-9A-Za-z]+:/ ) if(ip6) ip6=ip6 "," x[2]; else ip6=x[2];}
        printf "%s %s %s", d, ip4, ip6}')

# returns DHCP information for all interfaces where lease renew time is less than and lease expire time is greater than system time
DHCP=`cat /var/lib/dhcp/dhclient.*.leases | egrep 'interface |dhcp-server|lease-time|renew|expire' | \
        gawk '{if ( $0 ~ /expire/) printf "%s\n", $0; else printf "%s", $0}' | \
        awk 'match($0, /^.+?renew\s\S+\s([^ ]+)\s([^;]+).+?expire\s\S+\s([^ ]+)\s([^;]+)/,date) {
                split(date[1],rdate,"/"); 
                split(date[2],rtime,":");
                split(date[3],edate,"/");
                split(date[4],etime,":");
                renew_epoch=mktime(rdate[1] " " rdate[2] " " rdate[3] " " rtime[1] " " rtime[2] " " rtime[3]);
                expire_epoch=mktime(edate[1] " " edate[2] " " edate[3] " " etime[1] " " etime[2] " " etime[3]);
                if ( expire_epoch > systime() && renew_epoch < systime() ) print $0 "|" }'`

# returns address information for each available network interfaces and creates kv event
for x in `ip link show | egrep '^[0-9]' | cut -d: -f2`
do
        ip addr show dev $x | tr '\n' ' ' | \
        gawk -v dhcp="$DHCP" -v domain="$DOMAIN" -v ipv4dns="$IPv4DNS" -v ipv6dns="$IPv6DNS" -v dni="$DNI" -v server="$(hostname)" '
                {match($0, /^([^:]+):\s([^:]+).+?state\s(\S+).+?link\S+\s(\S+).+?inet\s([^\/]+).(\S+).+?scope\s(\S+)\s\S+\s+\S+\s(\S+)\s\S+\s(\S+)\s+\S+\s([^\/]+).(\S+)\s\S+\s(\S+)\s+\S+\s(\S+)\s\S+\s(\S+)/, val) 
                split("InterfaceIndex,InterfaceAlias,Status,MacAddress,IPv4Address,IPv4PrefixLength,IPv4Connectivity,IPv4ValidLifetime,IPv4PreferredLifetime,IPv6Address,IPv6PrefixLength,IPv6Connectivity,IPv6ValidLifetime,IPv6PreferredLifetime",key,",");
                if ( val[2] == dni ) is_default="true"; else is_default="false";
                split(dhcp,dhcp_entry,"|");
                for(i = 1; i <= length(dhcp_entry); i++) {
                        match(dhcp_entry[i], /^\s*\S+\s\"?([^ \"]+).+?time\s([^;]+).+?identifier\s([^;]+);\s+\S+\s\S+\s([^;]+);\s+\S+\s\S+\s([^;]+)/, dhcp_rec);
                        dhcp_lease_time=dhcp_rec[2]; dhcp_server=dhcp_rec[3]; dhcp_renew_time=dhcp_rec[4]; dhcp_expire_time=dhcp_rec[5];
                        if ( val[2] == dhcp_rec[1] ) break;
                }
                printf  strftime("%Y-%m-%dT%H:%M:%S%z", systime()) " Server=\"%s\" IsDefault=\"%s\" Domain=\"%s\" IPv4DNS=\"%s\" IPv6DNS=\"%s\" DHCPServers=\"%s\" DHCPLeaseTime=\"%s\" DHCPRenewTime=\"%s\" DHCPExpireTime=\"%s\" ", server, is_default, domain, ipv4dns, ipv6dns, dhcp_server, dhcp_lease_time, dhcp_renew_time, dhcp_expire_time; 

                for(i = 1; i <= length(key); i++) {
                        printf "%s=\"%s\" ", key[i], val[i]
                }
                } END {printf "\n"}'

done
